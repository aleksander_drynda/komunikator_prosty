#ifndef CONTACTS_H
#define CONTACTS_H

#include <contact.h>
#include <QVector>

class contacts
{
public:
    contacts();
    ~contacts();
    bool addContact(QString nick, QString userIP);
    void deleteContact(int arrayID, bool isDeleteAll = false);
    contact userData(int arrayID);
    QVector < contact > getContacts();
private:
    QVector < contact > contactList;
private:

};



#endif // CONTACTS_H
