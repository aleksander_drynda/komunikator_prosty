#ifndef EMOJI_H
#define EMOJI_H

#include <QString>

class Emoji
{
public:
    Emoji();
    static void decode(QString *text);
};

#endif // EMOJI_H
