#include "conversations.h"

Conversations::Conversations()
{
    conversationList.clear();
}

bool Conversations::addConversation(QString name, QVector <contact> contacts){
    for(int i=0; i<conversationList.count(); i++){
        if(conversationList[i].name == name){
            return false;
        }
    }
    QString id;
    conversation newConversation;
    newConversation.id = generate_id();
    newConversation.name = name;
    newConversation.contacts = contacts;
    conversationList.append(newConversation);
    return true;
}

void Conversations::addConversationFromBase(conversation newConversation){
    conversationList.append(newConversation);
}

QString Conversations::generate_id(){
    QString localhostIP="";
   /* const QHostAddress &localhost = QHostAddress(QHostAddress::LocalHost);
    for (const QHostAddress &address: QNetworkInterface::allAddresses()) {
        if (address.protocol() == QAbstractSocket::IPv4Protocol && address != localhost)
            // qDebug() << address.toString();
            localhostIP = address.toString();
    }//*/
    localhostIP+=QDateTime::currentDateTime().toString("yyyyMMddhhmmsszzz");
    return localhostIP;
}

void Conversations::deleteConversation(int arrayID){
    conversationList.removeAt(arrayID);
}

conversation* Conversations::getConversation(int arrayID){
    //conversationList.removeAt(arrayID);
    return &conversationList[arrayID];
}

QVector < conversation > Conversations::getConversations(){
    return conversationList;
}
