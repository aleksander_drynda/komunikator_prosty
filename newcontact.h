#ifndef NEWCONTACT_H
#define NEWCONTACT_H

#include <QDialog>
#include "widget.h"
#include "contacts.h"

namespace Ui {
class NewContact;
}

class NewContact : public QDialog
{
    Q_OBJECT

public:
    explicit NewContact(QWidget *parent = nullptr);
    ~NewContact();
    void set_widget_pointer(Widget *np);

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_textEdit_textChanged();

    void on_ip1_textChanged();

    void on_ip2_textChanged();

    void on_ip3_textChanged();

    void on_ip4_textChanged();

private:
    Widget *p=nullptr;
    Ui::NewContact *ui;
    contact newContact;
};

#endif // NEWCONTACT_H
