#include "login.h"
#include "ui_login.h"
#include <QMessageBox>

Login::Login(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Login)
{
    ui->setupUi(this);
    widget = new Widget();
    connect(widget, &Widget::popToLogin, this, &Login::show);
}

Login::~Login()
{
    delete ui;
}

void Login::on_pushButton_login_clicked()
{
    QString userName = ui->lineEdit_userName->text();
    QString password = ui->lineEdit_password->text();

    if((userName == "test" && password=="test") || (userName == "Patryk" && password=="test") ) {

        widget->show();
        this->close();
        ui->lineEdit_userName->clear();
        ui->lineEdit_password->clear();
        widget->nick = userName; 
    } else {
        QMessageBox::information(this,"Komunikator", "Niepoprawne login i hasło");
    }
}

void Login::on_pushButton_clicked()
{
    close();
}
