#ifndef NEWCONVERSATION_H
#define NEWCONVERSATION_H

#include <QDialog>
//#include <QHostAddress>
#include "contacts.h"
#include "widget.h"
#include "conversation.h"

namespace Ui {
class NewConversation;
}

class NewConversation : public QDialog
{
    Q_OBJECT

public:
    explicit NewConversation(QWidget *parent = nullptr);
    ~NewConversation();
    void set_widget_pointer(Widget *np);
    void set_contact_view(QVector <contact> Contacts);
    QVector <contact> tmpContacts;

private slots:
    void on_add_clicked();

    //void on_back_clicked();

    void on_save_clicked();

    void on_deny_clicked();


private:
    Ui::NewConversation *ui;
    Widget *p=nullptr;
    conversation newConversation;
    QVector <contact> newContacts;
};

#endif // NEWCONVERSATION_H
