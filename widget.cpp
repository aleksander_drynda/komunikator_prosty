#include "widget.h"
#include "ui_widget.h"
#include "newcontact.h"
#include "newconversation.h"
#include <iostream>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    readContactsXMLFile(contactFile);
    readConvosXMLFile(conversationFile);
    ui->setupUi(this);
    if(!isMock){
        ui->mockToolButton->hide();
    }
    ui->contactsListWidget->hide();
    ui->conversationsListWidget->hide();
    //9px odstepu
    //ui->pushButton_2->move(10,40);
    //ui->conversationsListWidget->move(10,70);
    ui->pushButton_2->move(10,ui->pushButton_2->y()-ui->contactsListWidget->height()-9);
    ui->conversationsListWidget->move(10,ui->conversationsListWidget->y()-ui->contactsListWidget->height()-9);
    ui->addConversationToolButton->move(160,ui->addConversationToolButton->y()-ui->contactsListWidget->height()-9);
    ui->deleteConversationToolButton->move(190,ui->deleteConversationToolButton->y()-ui->contactsListWidget->height()-9);

    ui->chatBrowser->hide();
    ui->chatEdit->hide();
    for(int i=0; i < contactList.getContacts().count(); i++){
        ui->contactsListWidget->addItem(contactList.userData(i).nick);
    }
    for(int i=0; i < conversationList.getConversations().count(); i++){
        ui->conversationsListWidget->addItem(conversationList.getConversation(i)->name);
    }
}

Widget::~Widget()
{
    delete ui;
}


void Widget::on_sendButton_clicked()//wysyłanie wiadomości
{
    QString date = QDateTime::currentDateTime().toString(dateAndTimeFormat);
    QString text = ui->chatEdit->toPlainText();
    if(!text.isEmpty()){
        text.remove('\n');

        Emoji::decode(&text);

        ui->chatBrowser->append(text);
        //ui->chatBrowser->append(generate_id());
        ui->chatEdit->clear();
        message tmp;
        tmp.body=text;
        tmp.date_time = date;
        tmp.author.nick = "Me";
        setFootStamp(tmp);
        currentConversation->messages.append(tmp);
    }
}


void Widget::on_chatEdit_textChanged()//wpisywanie treści wiadomości
{
    QString text = ui->chatEdit->toPlainText();
    if(text.contains("\n")){
        if(text.count() >= 2) {
            this->on_sendButton_clicked();
        } else {
            text.clear();
            ui->chatEdit->clear();
        }
    }
}

void Widget::on_exitToolButton_clicked()//wyjście z aplikacji
{
    switch( QMessageBox::question(
                this,
                tr("Komunikator"),
                //tr("Czy jestes pewny ze chcesz sie wylogowac?"),
                tr("Czy jestes pewnyze chcesz wyjsc?"),
                QMessageBox::Yes |
                QMessageBox::No) )
    {
    case QMessageBox::Yes:
        saveConvosXMLFile(conversationFile);
        this->nick = nullptr;
        ui->chatBrowser->clear();
        ui->contactsListWidget->clear();
        ui->conversationsListWidget->clear();
        contactList.deleteContact(0, true);
        this->close();
        //emit popToLogin();
        break;
    default:
        break;
    }
}

void Widget::on_addContactToolButton_clicked()//dodaj kontakt view
{
    NewContact w;
    w.set_widget_pointer(this);
    w.setModal(true);
    w.exec();
}

void Widget::add_contact(contact newContact)//dodaj kontakt func
{
    if(contactList.addContact(newContact.nick, newContact.adresIP) == true) {
        saveContactsXMLFile(contactFile);
        ui->contactsListWidget->addItem(newContact.nick);
    } else {
        QMessageBox::information(this,"Komunikator", "Podana nazwa uzytkownika juz istnieje.");
    }
}

void Widget::on_deleteContactToolButton_clicked()//usun kontakt func
{
    if(this->ui->contactsListWidget->selectedItems().size() != 0){
        int rowID = ui->contactsListWidget->currentRow();
        switch( QMessageBox::question(
                    this,
                    tr("Komunikator"),
                    tr("Czy jestes pewny ze chcesz usunac tego uzytkownika z listy kontakow?"),
                    QMessageBox::Yes |
                    QMessageBox::No) )
        {
        case QMessageBox::Yes:
            delete ui->contactsListWidget->takeItem(rowID);
            contactList.deleteContact(rowID);
            ui->contactsListWidget->clearSelection();
            saveContactsXMLFile(contactFile);
            break;
        default:
            break;
        }
    } else {
        QMessageBox::information(this,"Komunikator", "Nie wybrano kontaktu do usuniecia.");
    }
}

void Widget::setFootStamp(message message){ //ustawianie danych wyslania
    QString footStamp = message.author.nick + ", " + message.date_time + "\n";
    if(message.author.nick == "Me"){
        ui->chatBrowser->setTextColor(QColor( "blue" ));
        ui->chatBrowser->setFontPointSize(6);
        ui->chatBrowser->append(footStamp);
        ui->chatBrowser->setTextColor(QColor( "black" ));
        ui->chatBrowser->setFontPointSize(8);
    } else {
        ui->chatBrowser->setTextColor(QColor( "grey" ));
        ui->chatBrowser->setFontPointSize(6);
        ui->chatBrowser->append(footStamp);
        ui->chatBrowser->setTextColor(QColor( "black" ));
        ui->chatBrowser->setFontPointSize(8);
    }
}

void Widget::on_pushButton_clicked()//pokaż/ukryj listę kontaktów
{
    if(ui->contactsListWidget->isHidden()){
        ui->contactsListWidget->show();
        ui->pushButton_2->move(10,ui->pushButton_2->y()+ui->contactsListWidget->height()+9);
        ui->conversationsListWidget->move(10,ui->conversationsListWidget->y()+ui->contactsListWidget->height()+9);
        ui->addConversationToolButton->move(160,ui->addConversationToolButton->y()+ui->contactsListWidget->height()+9);
        ui->deleteConversationToolButton->move(190,ui->deleteConversationToolButton->y()+ui->contactsListWidget->height()+9);
    } else {
        ui->contactsListWidget->hide();
        ui->pushButton_2->move(10,ui->pushButton_2->y()-ui->contactsListWidget->height()-9);
        ui->conversationsListWidget->move(10,ui->conversationsListWidget->y()-ui->contactsListWidget->height()-9);
        ui->addConversationToolButton->move(160,ui->addConversationToolButton->y()-ui->contactsListWidget->height()-9);
        ui->deleteConversationToolButton->move(190,ui->deleteConversationToolButton->y()-ui->contactsListWidget->height()-9);
    }

}

void Widget::on_pushButton_2_clicked()//pokaż/ukryj listę grup
{
    if(ui->conversationsListWidget->isHidden()){
        ui->conversationsListWidget->show();
    } else {
        ui->conversationsListWidget->hide();
    }
}

void Widget::on_mockToolButton_clicked()
{
    mockVision = !mockVision;
}

void Widget::on_addConversationToolButton_clicked()//dodaj konwersacje
{
    NewConversation w;
    w.set_widget_pointer(this);
    w.set_contact_view(contactList.getContacts());
    w.setModal(true);
    w.exec();
}

void Widget::add_conversation(conversation newConversation)//dodaj konwersacje
{
    if(conversationList.addConversation(newConversation.name, newConversation.contacts) == true) {
        ui->conversationsListWidget->addItem(newConversation.name);
    } else {
        QMessageBox::information(this,"Komunikator", "Podana nazwa grupy juz istnieje.");
    }
}

void Widget::on_deleteConversationToolButton_clicked()//usun konwersacje
{
    if(this->ui->conversationsListWidget->selectedItems().size() != 0){
        int rowID = ui->conversationsListWidget->currentRow();
        switch( QMessageBox::question(
                    this,
                    tr("Komunikator"),
                    tr("Czy jestes pewny ze chcesz usunac ta konwersacje?"),
                    QMessageBox::Yes |
                    QMessageBox::No) )
        {
        case QMessageBox::Yes:
            delete ui->conversationsListWidget->takeItem(rowID);
            conversationList.deleteConversation(rowID);
            ui->conversationsListWidget->clearSelection();
            ui->chatBrowser->hide();
            ui->chatEdit->hide();
            //saveConvosXMLFile(conversationFile);
            break;
        default:
            break;
        }
    } else {
        QMessageBox::information(this,"Komunikator", "Nie wybrano kontaktu do usuniecia.");
    }
}

contact Widget::get_contact(int id)
{
    return contactList.userData(id);
    //return contactList[id];
}

void Widget::on_conversationsListWidget_itemClicked()
{
    if(this->ui->conversationsListWidget->selectedItems().size() != 0){
        int rowID = ui->conversationsListWidget->currentRow();
        currentConversation=conversationList.getConversation(rowID);
        ui->chatBrowser->clear();
        for(int i=0; i<currentConversation->messages.size(); i++){
            ui->chatBrowser->append(currentConversation->messages[i].body);
            setFootStamp(currentConversation->messages[i]);
        }
        if(ui->chatBrowser->isHidden()){
            ui->chatBrowser->show();
            ui->chatEdit->show();
        }
    }
}

void Widget::saveConvosXMLFile(QString filename)
{
    QFile file(filename);
    if(!file.exists()){
        file.setFileName("../komunikator_prosty/listaKonwersacji.xml");
    }
    file.open(QFile::WriteOnly);

    QXmlStreamWriter xmlWriter(&file);
    xmlWriter.setAutoFormatting(true);
    xmlWriter.writeStartDocument();

    xmlWriter.writeStartElement("LISTA_KONWERSACJI");
    for(int j=0; j<conversationList.getConversations().count(); j++){
        xmlWriter.writeStartElement("CONTACT_LIST");
        for(int i=0; i<conversationList.getConversation(j)->contacts.count(); i++){
            xmlWriter.writeTextElement("USER_NICK", conversationList.getConversation(j)->contacts[i].nick);
            xmlWriter.writeTextElement("IP_ADRESS", conversationList.getConversation(j)->contacts[i].adresIP);
        }
        xmlWriter.writeEndElement();
        xmlWriter.writeStartElement("MESSAGE_LIST");
        for(int i=0; i<conversationList.getConversation(j)->messages.count(); i++){
            xmlWriter.writeTextElement("SENDER_NICK", conversationList.getConversation(j)->messages[i].author.nick);
            xmlWriter.writeTextElement("BODY", conversationList.getConversation(j)->messages[i].body);
            xmlWriter.writeTextElement("DATE_TIME", conversationList.getConversation(j)->messages[i].date_time);
        }
        xmlWriter.writeEndElement();
        xmlWriter.writeTextElement("ID", conversationList.getConversation(j)->id);
        xmlWriter.writeTextElement("NAME", conversationList.getConversation(j)->name);
    }

    xmlWriter.writeEndElement();

    file.close();
}

void Widget::saveContactsXMLFile(QString filename)
{
    QFile file(filename);
    if(!file.exists()){
        file.setFileName("../komunikator_prosty/listaKontaktow.xml");
    }
    file.open(QFile::WriteOnly);

    QXmlStreamWriter xmlWriter(&file);
    xmlWriter.setAutoFormatting(true);
    xmlWriter.writeStartDocument();

    xmlWriter.writeStartElement("LISTA_KONTAKTOW");
    for(int i=0; i<contactList.getContacts().count(); i++){
        xmlWriter.writeTextElement("NICK", get_contact(i).nick);
        xmlWriter.writeTextElement("IP_ADRESS", get_contact(i).adresIP);
    }

    xmlWriter.writeEndElement();

    file.close();
}

void Widget::readConvosXMLFile(QString filename)
{
    QXmlStreamReader Rxml;
    conversation newConversation;
    contact newContact;
    message newMessage;
    QFile file(filename);
    if (!file.open(QFile::ReadOnly | QFile::Text))
    {
        std::cerr << "Error: Cannot read file " << qPrintable(filename)
                  << ": " << qPrintable(file.errorString())
                  << std::endl;

    }

    Rxml.setDevice(&file);
    Rxml.readNext();

    while(!Rxml.atEnd())
    {
        if(Rxml.isStartElement())
        {
            if(Rxml.name() == "LISTA_KONWERSACJI")
            {
                while(!Rxml.atEnd())
                {
                    if(Rxml.isEndElement())
                    {
                        Rxml.readNext();
                        break;
                    }
                    else if(Rxml.isCharacters())
                    {
                        Rxml.readNext();
                    }
                    else if(Rxml.isStartElement())
                    {
                        if(Rxml.name() == "CONTACT_LIST")
                        {
                            while(!Rxml.atEnd())
                            {
                                if(Rxml.isEndElement())
                                {
                                    Rxml.readNext();
                                    break;
                                }
                                else if(Rxml.isCharacters())
                                {
                                    Rxml.readNext();
                                }
                                else if(Rxml.isStartElement())
                                {
                                    if(Rxml.name() == "USER_NICK")
                                    {
                                        newContact.nick = Rxml.readElementText();
                                    }
                                    else if(Rxml.name() == "IP_ADRESS")
                                    {
                                        newContact.adresIP = Rxml.readElementText();
                                        newConversation.contacts.append(newContact);
                                    }
                                    Rxml.readNext();
                                }
                                else
                                {
                                    Rxml.readNext();
                                }
                            }

                        }
                        else if(Rxml.name() == "MESSAGE_LIST")
                        {
                            while(!Rxml.atEnd())
                            {
                                if(Rxml.isEndElement())
                                {
                                    Rxml.readNext();
                                    break;
                                }
                                else if(Rxml.isCharacters())
                                {
                                    Rxml.readNext();
                                }
                                else if(Rxml.isStartElement())
                                {
                                    if(Rxml.name() == "SENDER_NICK")
                                    {
                                        newMessage.author.nick = Rxml.readElementText();
                                        newMessage.author.adresIP = nullptr;
                                    }
                                    else if(Rxml.name() == "BODY")
                                    {
                                        newMessage.body = Rxml.readElementText();
                                    }
                                    else if(Rxml.name() == "DATE_TIME")
                                    {
                                        newMessage.date_time = Rxml.readElementText();
                                        newConversation.messages.append(newMessage);
                                    }
                                    Rxml.readNext();
                                }
                                else
                                {
                                    Rxml.readNext();
                                }
                            }

                        }
                        else if(Rxml.name() == "ID")
                        {
                            newConversation.id = Rxml.readElementText();
                        }
                        else if(Rxml.name() == "NAME")
                        {
                            newConversation.name = Rxml.readElementText();
                            conversationList.addConversationFromBase(newConversation);
                            newConversation.id = nullptr;
                            newConversation.name = nullptr;
                            newConversation.contacts.clear();
                            newConversation.messages.clear();
                        }
                        Rxml.readNext();
                    }
                    else
                    {
                        Rxml.readNext();
                    }
                }
            }
        }
        else
        {
            Rxml.readNext();
        }

        file.close();

        if (Rxml.hasError())
        {
            std::cerr << "Error: Failed to parse file "
                 << qPrintable(filename) << ": "
                 << qPrintable(Rxml.errorString()) << std::endl;
        }
        else if (file.error() != QFile::NoError)
        {
            std::cerr << "Error: Cannot read file " << qPrintable(filename)
                      << ": " << qPrintable(file.errorString())
                      << std::endl;
        }
    }
}

void Widget::readContactsXMLFile(QString filename)
{
    QXmlStreamReader Rxml;
    contact newContact;
    QFile file(filename);
    if (!file.open(QFile::ReadOnly | QFile::Text))
    {
        std::cerr << "Error: Cannot read file " << qPrintable(filename)
                  << ": " << qPrintable(file.errorString())
                  << std::endl;

    }

    Rxml.setDevice(&file);
    Rxml.readNext();

    while(!Rxml.atEnd())
    {
        if(Rxml.isStartElement())
        {
            if(Rxml.name() == "LISTA_KONTAKTOW")
            {
                while(!Rxml.atEnd())
                {
                    if(Rxml.isEndElement())
                    {
                        Rxml.readNext();
                        break;
                    }
                    else if(Rxml.isCharacters())
                    {
                        Rxml.readNext();
                    }
                    else if(Rxml.isStartElement())
                    {
                        if(Rxml.name() == "NICK")
                        {
                            newContact.nick = Rxml.readElementText();
                        }
                        else if(Rxml.name() == "IP_ADRESS")
                        {
                            newContact.adresIP = Rxml.readElementText();
                            contactList.addContact(newContact.nick, newContact.adresIP);
                        }
                        Rxml.readNext();
                    }
                    else
                    {
                        Rxml.readNext();
                    }
                }
            }
        }
        else
        {
            Rxml.readNext();
        }

        file.close();

        if (Rxml.hasError())
        {
            std::cerr << "Error: Failed to parse file "
                 << qPrintable(filename) << ": "
                 << qPrintable(Rxml.errorString()) << std::endl;
        }
        else if (file.error() != QFile::NoError)
        {
            std::cerr << "Error: Cannot read file " << qPrintable(filename)
                      << ": " << qPrintable(file.errorString())
                      << std::endl;
        }
    }
}
