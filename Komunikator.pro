QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    contacts.cpp \
    conversations.cpp \
    emoji.cpp \
    login.cpp \
    main.cpp \
    newcontact.cpp \
    newconversation.cpp \
    widget.cpp

HEADERS += \
    contact.h \
    contacts.h \
    conversation.h \
    conversations.h \
    emoji.h \
    login.h \
    message.h \
    newcontact.h \
    newconversation.h \
    widget.h

FORMS += \
    login.ui \
    newcontact.ui \
    newconversation.ui \
    widget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    Emotka5v112.png \
    close-512.webp \
    minus+icon-1320185727443878037.png \
    plus-1768091-1502264.png
