#include "contacts.h"

contacts::contacts(){
    contactList.clear();
}

contacts::~contacts(){
    contactList.clear();
}

bool contacts::addContact(QString nick, QString userIP){
    for(int i=0; i<contactList.count(); i++){
        if(contactList[i].nick == nick){
            return false;
        }
    }
    contact newContact;
    newContact.nick = nick;
    newContact.adresIP = userIP;
    contactList.append(newContact);
    return true;
}

void contacts::deleteContact(int arrayID, bool isDeleteAll){
    if(isDeleteAll) {
        contactList.clear();
    } else {
        contactList.removeAt(arrayID);
    }
}

contact contacts::userData(int arrayID){
    return contactList[arrayID];
}

QVector < contact > contacts::getContacts(){
    return contactList;
}
